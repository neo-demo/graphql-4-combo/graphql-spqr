package com.graphql.demo.entity;

import io.leangen.graphql.annotations.GraphQLId;
import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class Greetings {

    @GraphQLId
    private int id;

    @GraphQLQuery(name = "greetings", description = "greetings to be given")
    private String greeting;
}
