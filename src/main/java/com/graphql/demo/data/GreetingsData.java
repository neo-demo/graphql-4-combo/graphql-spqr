package com.graphql.demo.data;


import com.graphql.demo.entity.Greetings;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class GreetingsData {
    public List<Greetings> greetingsList = new ArrayList<>(
        Arrays.asList(
            Greetings.builder().id(1).greeting("Hi").build(),
            Greetings.builder().id(2).greeting("Hello").build(),
            Greetings.builder().id(3).greeting("Welcome").build(),
            Greetings.builder().id(4).greeting("HO! HO! HO!").build()
        )
    );
}
