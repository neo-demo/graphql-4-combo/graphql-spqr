package com.graphql.demo.resolver;

import com.graphql.demo.data.GreetingsData;
import com.graphql.demo.entity.Greetings;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Service
@GraphQLApi
public class Resolver {


    private final GreetingsData greetingsData;

    public Resolver(GreetingsData greetingsData) {
        this.greetingsData = greetingsData;
    }

    @GraphQLQuery()
    public List<Greetings> greetingsList() {
        return greetingsData.greetingsList;
    }

    @GraphQLQuery()
    public Optional<Greetings> findGreetingById(@GraphQLArgument(name = "id") int id) {
        return greetingsData.greetingsList
                .parallelStream()
                .filter(greetings -> greetings.getId() == id).findFirst();
    }

    @GraphQLMutation()
    public Greetings createGreetings(@GraphQLArgument(name = "greeting") String  greetings) {
        greetingsData.greetingsList.add(
                new Greetings(greetingsData.greetingsList.size() + 1, greetings)
        );
        return greetingsData.greetingsList.get(greetingsData.greetingsList.size()-1);
    }

    @GraphQLMutation()
    public String deleteGreetings(@GraphQLArgument(name = "id") int id) {
        AtomicReference<String> response = new AtomicReference<>("Requested greetings id not found ");
        Optional<Greetings> foundGreeting = greetingsData.greetingsList
                .stream()
                .filter(greetings -> greetings.getId() == id)
                .findFirst();
        foundGreeting
                .ifPresent(greetings -> {
                    greetingsData.greetingsList.remove(greetings);
                    response.set("Greetings removed with id: " + foundGreeting.get().getId());
        });
        return response.get();
    }
}
